package game;

import gameObjects.Box;

public class GameObjectSetter {

    GameScreen gameScreen;
    public int boxNumberL1;
    public int boxNumberL2;

    public GameObjectSetter(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    public void setBoxes() {
        int level = 1;
        gameScreen.gameObjects[0][0] = new Box();
        gameScreen.gameObjects[0][0].fieldX = 2 * gameScreen.fieldSize;
        gameScreen.gameObjects[0][0].fieldY = 2 * gameScreen.fieldSize;
        boxNumberL1++;

        gameScreen.gameObjects[0][1] = new Box();
        gameScreen.gameObjects[0][1].fieldX = 7 * gameScreen.fieldSize;
        gameScreen.gameObjects[0][1].fieldY = 2 * gameScreen.fieldSize;
        boxNumberL1++;

        gameScreen.gameObjects[0][2] = new Box();
        gameScreen.gameObjects[0][2].fieldX = 3 * gameScreen.fieldSize;
        gameScreen.gameObjects[0][2].fieldY = 7 * gameScreen.fieldSize;
        boxNumberL1++;

        gameScreen.gameObjects[0][3] = new Box();
        gameScreen.gameObjects[0][3].fieldX = 5 * gameScreen.fieldSize;
        gameScreen.gameObjects[0][3].fieldY = 5 * gameScreen.fieldSize;
        boxNumberL1++;

        gameScreen.gameObjects[0][4] = new Box();
        gameScreen.gameObjects[0][4].fieldX = 3 * gameScreen.fieldSize;
        gameScreen.gameObjects[0][4].fieldY = 8 * gameScreen.fieldSize;
        boxNumberL1++;


        level = 1;
        gameScreen.gameObjects[1][0] = new Box();
        gameScreen.gameObjects[1][0].fieldX = 5 * gameScreen.fieldSize;
        gameScreen.gameObjects[1][0].fieldY = 5 * gameScreen.fieldSize;
        boxNumberL2++;

        gameScreen.gameObjects[1][1] = new Box();
        gameScreen.gameObjects[1][1].fieldX = 7 * gameScreen.fieldSize;
        gameScreen.gameObjects[1][1].fieldY = 5 * gameScreen.fieldSize;
        boxNumberL2++;

        gameScreen.gameObjects[1][2] = new Box();
        gameScreen.gameObjects[1][2].fieldX = 6 * gameScreen.fieldSize;
        gameScreen.gameObjects[1][2].fieldY = 7 * gameScreen.fieldSize;
        boxNumberL2++;

        gameScreen.gameObjects[1][3] = new Box();
        gameScreen.gameObjects[1][3].fieldX = 7 * gameScreen.fieldSize;
        gameScreen.gameObjects[1][3].fieldY = 7 * gameScreen.fieldSize;
        boxNumberL2++;
    }
}
