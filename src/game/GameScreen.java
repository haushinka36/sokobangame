package game;

import fields.BoardSetter;
import gameObjects.Character;
import gameObjects.GameObject;
import gui.GUI;

import javax.swing.*;
import java.awt.*;

public class GameScreen extends JPanel implements Runnable {

    // Screen Settings = Konstanzen?
    public final int originalFieldSize = 16;    // Grösse eines Felds in Pixel
    public final int scale = 3;
    public final int fieldSize = originalFieldSize * scale; // 48x48 fields (orginal * scale)
    public final int maxScreenHorizontal = 10;
    public final int maxScreenVertical = 10;
    public final int maxBoard = 20;
    public final int FPS = 60;
    public final int screenWidth = fieldSize * maxScreenHorizontal;
    public final int screenHeight = fieldSize * maxScreenVertical;

    // Wechseln des Levels
    public int currentBoard = 1;
    public int gameState;
    public final int playState = 1;
    public final int pauseState = 2;
    public final int titleState = 3;

    // System
    public BoardSetter boardSetter = new BoardSetter(this);   // Felder werden gezeichnet
    KeyHandler keyHandler = new KeyHandler(this);
    Sound soundEffect = new Sound();
    Sound music = new Sound();
    public CollisionHandler collisionHandler = new CollisionHandler(this);
    public GameObjectSetter gameObjectSetter = new GameObjectSetter(this);
    public GUI gameScreen = new GUI(this);
    public Thread gameThread;           // muss Runnable implementieren, damit Thread gebraucht werden kann
                                        // kann gestartet und gestoppt werden
                                        // damit das Spiel ständing am laufen ist...


    // GameObjects
    public Character character = new Character(this, keyHandler);

    public GameObject[][] gameObjects = new GameObject[maxBoard][10];

    public GameScreen() {
        this.setPreferredSize(new Dimension(screenWidth, screenHeight));
        this.setBackground(Color.BLACK);
        this.setDoubleBuffered(true);   // für bessere Rendering Performance
        this.addKeyListener(keyHandler);
        this.setFocusable(true);        // JPanel kann so KeyInput entgegennehmen

    }

    public void setUpBoard() {
        gameObjectSetter.setBoxes();
        playMusic(1);
        stopMusic();
        gameState = playState;
    }

    public void startGameThread() {     // so wird ein Thread instanziiert
        gameThread = new Thread(this);
        gameThread.start();
    }

    @Override
    public void run() {
        // Gameloop
        double drawInterval = 1000000000 / FPS;
        double delta = 0;
        long lastTime = System.nanoTime();
        long currentTime;

        while (gameThread != null) {     // jedes Mal, wenn sich eine Info verändert...
            currentTime = System.nanoTime();   // jetzige Zeit, damit bestimmt werden kann, wann wieder update() und repaint()

            delta += (currentTime - lastTime) / drawInterval;

            lastTime = currentTime;     // neu setzten der jetzigen Zeit

            if (delta >= 1) {
                update();               // updatet Informationen z.B. Spieler Position
                repaint();              // (Methode von paintComponent) zeichnet den Screen mit updateter Informationen neu
                delta--;
            }
        }
    }

    public void update() {

        if (gameState == playState) {
            character.update();
            // Falls update Methode für gameObjects implementiert wird
//        for (int i = 0; i < gameObjects[1].length; i++) {
//            if (gameObjects[currentBoard][i] != null) {
//                gameObjects[currentBoard][i].update();
//            }
//        }
        }

        if (gameState == pauseState) {
            // nichts passiert, weil pausiert
        }

    }

    public void paintComponent(Graphics g) {    // Standard Zeichnen-Methode von JPanel
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;         // hat mehr Funktionen als Graphics (beide werden gebraucht)

        boardSetter.draw(g2);                            // zuerst Feld, danach Boxen, danach Character
        for (int i = 0; i < gameObjects[1].length; i++) {
            if (gameObjects[currentBoard][i] != null) {
                gameObjects[currentBoard][i].draw(g2, this);
            }
        }
        character.draw(g2);
        gameScreen.draw(g2);
        g2.dispose();
    }

    public void playMusic(int i) {
        music.setFile(i);
        music.play();
        music.loop();
    }

    public void stopMusic() {
        music.stop();
    }

    public void playSoundEffect(int i) {
        soundEffect.setFile(i);
        soundEffect.play();
    }
}
