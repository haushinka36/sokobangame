package game;

import gameObjects.GameObject;

public class CollisionHandler {

    GameScreen gameScreen;

    public CollisionHandler(GameScreen gameScreen) {
        this.gameScreen = gameScreen;
    }

    public void checkField(GameObject gameObject) {

        int hitBoxLeftX = gameObject.x + gameObject.hitBox.x;
        int hitBoxRightX = gameObject.x + gameObject.hitBox.x + gameObject.hitBox.width;
        int hitBoxTopY = gameObject.y + gameObject.hitBox.y;
        int hitBoxBottomY = gameObject.y + gameObject.hitBox.y + gameObject.hitBox.height;

        int figureLeftCol = hitBoxLeftX / gameScreen.fieldSize;
        int figureRightCol = hitBoxRightX / gameScreen.fieldSize;
        int figureTopRow = hitBoxTopY / gameScreen.fieldSize;
        int figureBottomRow = hitBoxBottomY / gameScreen.fieldSize;

        int firstField, secondField;

        switch (gameObject.direction) {
            case "up" -> {
                figureTopRow = (hitBoxTopY - gameObject.speed) / gameScreen.fieldSize;
                firstField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureLeftCol][figureTopRow];
                secondField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureRightCol][figureTopRow];
                if (gameScreen.boardSetter.fields[firstField].collision || gameScreen.boardSetter.fields[secondField].collision) {
                    gameObject.collided = true;
                    gameScreen.gameScreen.showMessage("You hit the wall!");
                }
            }
            case "down" -> {
                figureBottomRow = (hitBoxBottomY + gameObject.speed) / gameScreen.fieldSize;
                firstField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureLeftCol][figureBottomRow];
                secondField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureRightCol][figureBottomRow];
                if (gameScreen.boardSetter.fields[firstField].collision || gameScreen.boardSetter.fields[secondField].collision) {
                    gameObject.collided = true;
                    gameScreen.gameScreen.showMessage("You hit the wall!");
                }
            }
            case "left" -> {
                figureLeftCol = (hitBoxLeftX - gameObject.speed) / gameScreen.fieldSize;
                firstField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureLeftCol][figureTopRow];
                secondField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureLeftCol][figureBottomRow];
                if (gameScreen.boardSetter.fields[firstField].collision || gameScreen.boardSetter.fields[secondField].collision) {
                    gameObject.collided = true;
                    gameScreen.gameScreen.showMessage("You hit the wall!");
                }
            }
            case "right" -> {
                figureRightCol = (hitBoxRightX + gameObject.speed) / gameScreen.fieldSize;
                firstField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureRightCol][figureTopRow];
                secondField = gameScreen.boardSetter.gameBoard[gameScreen.currentBoard][figureRightCol][figureBottomRow];
                if (gameScreen.boardSetter.fields[firstField].collision || gameScreen.boardSetter.fields[secondField].collision) {
                    gameObject.collided = true;
                    gameScreen.gameScreen.showMessage("You hit the wall!");
                }
            }

        }

        gameObject.hitBox.width = 42;
        gameObject.hitBox.height = 42;

    }

    // es wird geprüft, ob Charakter ein Gameobject berührt und wenn ja, gibt es Index des Objekts zurück
    public int checkObject(GameObject gameObject, boolean character) {

        int index = 999;

        for (int i = 0; i < gameScreen.gameObjects[1].length; i++) {
            if (gameScreen.gameObjects[gameScreen.currentBoard][i] != null) {
                // wo sich HitBox von Charakter befindet
                gameObject.hitBox.x = gameObject.x + gameObject.hitBox.x;
                gameObject.hitBox.y = gameObject.y + gameObject.hitBox.y;
                // wo sich HitBox von Objekt befindet
                gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.x = gameScreen.gameObjects[gameScreen.currentBoard][i].fieldX + gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.x;
                gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.y = gameScreen.gameObjects[gameScreen.currentBoard][i].fieldY + gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.y;

                switch (gameObject.direction) {
                    case "up" -> {
                        gameObject.hitBox.y -= gameObject.speed;
                        if (gameObject.hitBox.intersects(gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox)) {
                            if (gameScreen.gameObjects[gameScreen.currentBoard][i].collision) {
                                gameScreen.playSoundEffect(3);
                                gameObject.collided = true;
                                System.out.println("UP collision");
                                gameScreen.gameObjects[gameScreen.currentBoard][i].fieldY -= gameObject.speed;
                                gameObject.hitBox.height += gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.height;
                            }
                        }

                    }
                    case "down" -> {
                        gameObject.hitBox.y += gameObject.speed;
                        if (gameObject.hitBox.intersects(gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox)) {
                            if (gameScreen.gameObjects[gameScreen.currentBoard][i].collision) {
                                gameScreen.playSoundEffect(3);
                                gameObject.collided = true;
                                System.out.println("DOWN collision");
                                gameScreen.gameObjects[gameScreen.currentBoard][i].fieldY += gameObject.speed;
                                gameObject.hitBox.height += gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.height;
                            }
                        }
                    }
                    case "left" -> {
                        gameObject.hitBox.x -= gameObject.speed;
                        if (gameObject.hitBox.intersects(gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox)) {
                            if (gameScreen.gameObjects[gameScreen.currentBoard][i].collision) {
                                gameScreen.playSoundEffect(3);
                                gameObject.collided = true;
                                System.out.println("LEFT collision");
                                gameScreen.gameObjects[gameScreen.currentBoard][i].fieldX -= gameObject.speed;
                                gameObject.hitBox.width += gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.width;
                            }
                        }
                    }
                    case "right" -> {
                        gameObject.hitBox.x += gameObject.speed;
                        if (gameObject.hitBox.intersects(gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox)) {
                            if (gameScreen.gameObjects[gameScreen.currentBoard][i].collision) {
                                gameScreen.playSoundEffect(3);
                                gameObject.collided = true;
                                System.out.println("RIGHT collision");
                                gameScreen.gameObjects[gameScreen.currentBoard][i].fieldX += gameObject.speed;
                                gameObject.hitBox.width += gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.width;
                            }
                        }
                    }
                }

                gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.x = gameScreen.gameObjects[gameScreen.currentBoard][i].hitBoxDefaultX;
                gameScreen.gameObjects[gameScreen.currentBoard][i].hitBox.y = gameScreen.gameObjects[gameScreen.currentBoard][i].hitBoxDefaultY;
                gameObject.hitBox.x = gameObject.hitBoxDefaultX;
                gameObject.hitBox.y = gameObject.hitBoxDefaultY;
            }
        }
        return index;
    }
}
