package game;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;
import java.net.URL;

public class Sound {

    Clip clip; // wav Dateien importieren
    URL soundURL[] = new URL[4];

    public Sound() {
        soundURL[0] = getClass().getResource("/resources/sound/level.wav");
        soundURL[1] = getClass().getResource("/resources/sound/menu.wav");
        soundURL[2] = getClass().getResource("/resources/sound/box-move.wav");
        soundURL[3] = getClass().getResource("/resources/sound/goal.wav");
    }

    // ähnlich wie beim Einlesen der Levels
    public void setFile(int i) {
        try {
            AudioInputStream stream = AudioSystem.getAudioInputStream(soundURL[i]);
            clip = AudioSystem.getClip();
            clip.open(stream);
            FloatControl control = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
            control.setValue(-30.0f); // reduziert Lautstärke um 30 Dezibel
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void play() {
        clip.start();
    }

    public void loop() {
        clip.loop(Clip.LOOP_CONTINUOUSLY);
    }

    public void stop() {
        clip.stop();
    }
}
