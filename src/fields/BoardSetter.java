package fields;

import game.GameScreen;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class BoardSetter {

    GameScreen gameScreen;
    public Field[] fields;
    public int[][][] gameBoard;

    public BoardSetter(GameScreen gameScreen) {
        this.gameScreen = gameScreen;

        fields = new Field[20];
        gameBoard = new int[gameScreen.maxBoard][gameScreen.maxScreenHorizontal][gameScreen.maxScreenVertical];
        getFieldImage();
        fillGameBoard("/resources/levels/level1.txt", 0);
        fillGameBoard("/resources/levels/level2.txt", 1);
    }

    public void fillGameBoard(String level, int board) {
        try {
            InputStream is = getClass().getResourceAsStream(level);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));

            int column = 0;
            int row = 0;

            while (column < gameScreen.maxScreenHorizontal && row < gameScreen.maxScreenVertical) {

                String line = reader.readLine();

                while (column < gameScreen.maxScreenHorizontal) {

                    String[] numbers = line.split("");
                    int number = Integer.parseInt(numbers[column]);
                    gameBoard[board][column][row] = number;
                    column++;
                }

                if (column == gameScreen.maxScreenHorizontal) {
                    column = 0;
                    row++;
                }
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getFieldImage() {
        try {
            fields[0] = new Field();
            fields[0].image = ImageIO.read(getClass().getResourceAsStream("/resources/fields/wall.png"));
            fields[0].collision = true;

            fields[1] = new Field();
            fields[1].image = ImageIO.read(getClass().getResourceAsStream("/resources/fields/gravel.png"));

            fields[2] = new Field();
            fields[2].image = ImageIO.read(getClass().getResourceAsStream("/resources/objects/box.png"));
            fields[2].collision = true;

            fields[3] = new Field();
            fields[3].image = ImageIO.read(getClass().getResourceAsStream("/resources/fields/goal.png"));

            fields[4] = new Field();
            fields[4].image = ImageIO.read(getClass().getResourceAsStream("/resources/fields/goal.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void draw(Graphics2D g2) {

        // Zeichnensystem
        int y = 0;
        int x = 0;
        // den ganzen Screen abfüllen
        for (int i = 0; i < gameScreen.maxScreenHorizontal; i++) {
            for (int j = 0; j < gameScreen.maxScreenVertical; j++) {
                g2.drawImage(fields[gameBoard[gameScreen.currentBoard][j][i]].image, x, y, gameScreen.fieldSize, gameScreen.fieldSize, null);
                x += gameScreen.fieldSize;
            }
            x = 0;
            y += gameScreen.fieldSize;
        }
    }
}
