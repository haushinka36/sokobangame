package gameObjects;

import game.GameScreen;

import java.awt.*;
import java.awt.image.BufferedImage;

public abstract class GameObject {

    public BufferedImage image, firstUp, secondUp, firstDown, secondDown, firstLeft, secondLeft, firstRight, secondRight;
    public int spriteCounter = 0;
    public int spriteNumber = 1;

    public String name;
    public String direction;
    public boolean collision = false;
    public boolean collided = false;
    public Rectangle hitBox = new Rectangle(0, 0, 48, 48);
    public int hitBoxDefaultX = 0;
    public int hitBoxDefaultY = 0;

    public int fieldX, fieldY;
    public int x, y;
    public int speed;

    public void draw(Graphics2D g2, GameScreen gameScreen) {
        g2.drawImage(image, fieldX, fieldY, gameScreen.fieldSize, gameScreen.fieldSize, null);
    }
}
