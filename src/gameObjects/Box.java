package gameObjects;

import javax.imageio.ImageIO;
import java.io.IOException;

public class Box extends GameObject {

    public Box() {
        name = "Box";
        getBoxImage();
    }

    public void getBoxImage() {
        try {
            image = ImageIO.read(getClass().getResourceAsStream("/resources/objects/box.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        collision = true;
    }
}
