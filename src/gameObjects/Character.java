package gameObjects;

import game.GameScreen;
import game.KeyHandler;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Character extends GameObject {

    GameScreen gameScreen;
    KeyHandler keyHandler;
    boolean moving = false;
    int pixelCounter = 0;
    int standCounter = 0;

    public Character(GameScreen gameScreen, KeyHandler keyHandler) {
        this.gameScreen = gameScreen;
        this.keyHandler = keyHandler;

        // Rechteck, das als Hitbox fungiert
        hitBox = new Rectangle();
        hitBox.x = 5;
        hitBox.y = 5;
        hitBoxDefaultX = hitBox.x;
        hitBoxDefaultY = hitBox.y;
        hitBox.width = 42;
        hitBox.height = 42;

        setDefaultValues();
        getPlayerImage();
    }

    public void setDefaultValues() {
        x = gameScreen.fieldSize * 4; // character starting position
        y = gameScreen.fieldSize * 4;
        speed = 4;
        direction = "down";
    }

    public void getPlayerImage() {
        try {
            firstUp = ImageIO.read(getClass().getResourceAsStream("/resources/character/up1.png"));
            secondUp = ImageIO.read(getClass().getResourceAsStream("/resources/character/up2.png"));
            firstDown = ImageIO.read(getClass().getResourceAsStream("/resources/character/right1.png"));
            secondDown = ImageIO.read(getClass().getResourceAsStream("/resources/character/right2.png"));
            firstLeft = ImageIO.read(getClass().getResourceAsStream("/resources/character/left1.png"));
            secondLeft = ImageIO.read(getClass().getResourceAsStream("/resources/character/left2.png"));
            firstRight = ImageIO.read(getClass().getResourceAsStream("/resources/character/right1.png"));
            secondRight = ImageIO.read(getClass().getResourceAsStream("/resources/character/right2.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void update() {
//        if (!moving) {
// Sprite verändert sich nur, wenn bereits eine Taste gedrückt wurde
        if (keyHandler.up || keyHandler.down || keyHandler.left || keyHandler.right || keyHandler.win) {
            if (keyHandler.up) {
                direction = "up";
            }
            if (keyHandler.down) {
                direction = "down";
            }
            if (keyHandler.right) {
                direction = "right";
            }
            if (keyHandler.left) {
                direction = "left";
            }
            if (keyHandler.win) {
                direction = "win";
            }
//                moving = true;
            // berührt Charakter Feld mit hitBox?
            collided = false;
            gameScreen.collisionHandler.checkField(this);
            int objectIndex = gameScreen.collisionHandler.checkObject(this, true);
//        }
//            else {
//                standCounter++;
//                if (standCounter == 20) {
//                    spriteNumber = 1;
//                    standCounter = 0;
//                }
//            }
//        }
//        if (moving) {
        if (!collided) {
            switch (direction) {
                case "up" -> y -= speed;
                case "down" -> y += speed;
                case "left" -> x -= speed;
                case "right" -> x += speed;
                case "win" -> {
                    gameScreen.gameScreen.gameFinished = true;
                    gameScreen.stopMusic();
                    gameScreen.playSoundEffect(3);
                }
            }
        }
        spriteCounter++;
        if (spriteCounter > 20) {
            if (spriteNumber == 1) {
                spriteNumber = 2;
            } else if (spriteNumber == 2) {
                spriteNumber = 1;
            }
            spriteCounter = 0;
        }
//            pixelCounter += speed;
//            if (pixelCounter == 48) {
//                moving = false;
//                pixelCounter = 0;
//            }
        }
    }

    public void draw(Graphics2D g2) {
        BufferedImage image = switch (direction) {          // für die Laufanimation
            case "up" -> spriteNumber == 1 ? firstUp : secondUp;
            case "down" -> spriteNumber == 1 ? firstDown : secondDown;
            case "left" -> spriteNumber == 1 ? firstLeft : secondLeft;
            case "right" -> spriteNumber == 1 ? firstRight : secondRight;
            default -> null;
        };
        g2.drawImage(image, x, y, gameScreen.fieldSize, gameScreen.fieldSize, null);
        g2.setColor(Color.RED);
        // hitbox anzeigen
//        g2.drawRect(x + hitBox.x, y + hitBox.y, hitBox.width, hitBox.height);
    }
}
