import game.GameScreen;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {

        // Graphische Oberfläche
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.setTitle("Mario's Sokoban Game");

        GameScreen gameScreen = new GameScreen(); // JPanel + Runnable
        frame.add(gameScreen);

        frame.pack();

        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        gameScreen.setUpBoard();
        gameScreen.startGameThread();
    }
}