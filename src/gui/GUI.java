package gui;

import game.GameScreen;
import gameObjects.Box;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.text.DecimalFormat;

public class GUI {

    GameScreen gameScreen;
    Graphics2D g2;
    Font font40B, font25, font60;
    BufferedImage boxImage;
    DecimalFormat decimalFormat = new DecimalFormat("#0.00");
    private boolean messageOn = false;
    private String message = "";
    private int messageCounter = 0;
    public boolean gameFinished = false;
    private double playTime;
    private double marioCounter = 300;

    public GUI(GameScreen gameScreen){
        this.gameScreen = gameScreen;

        font40B = new Font("Arial", Font.BOLD, 40);
        font25 = new Font("Arial", Font.PLAIN, 25);
        font60 = new Font("Arial", Font.BOLD, 60);
        Box box = new Box();
        boxImage = box.image;
    }

    public void showMessage(String text) {
        message = text;
        messageOn = true;
    }

    public void draw(Graphics2D g2) {
        this.g2 = g2;

        if (gameFinished) {
            g2.setFont(font40B);
            g2.setColor(Color.WHITE);

            String text;
            int textLength;
            int x;
            int y;

            text = "Level completed!";
            textLength = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth(); // Länge des Texts
            x = gameScreen.screenWidth / 2 - textLength / 2;
            y = gameScreen.screenHeight / 2 - gameScreen.fieldSize;
            g2.drawString(text, x, y);

            text = "Your Time is: " + decimalFormat.format(playTime) + "!";
            textLength = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth(); // Länge des Texts
            x = gameScreen.screenWidth / 2 - textLength / 2;
            y = gameScreen.screenHeight / 2 + gameScreen.fieldSize*3;
            g2.drawString(text, x, y);


            g2.setFont(font60);
            g2.setColor(Color.YELLOW);

            text = "Congratulations!";
            textLength = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth();
            x = gameScreen.screenWidth / 2 - textLength / 2;
            y = gameScreen.screenHeight / 2 + gameScreen.fieldSize * 2;
            g2.drawString(text, x, y);

            gameScreen.gameThread = null;

        } else {
            if (gameScreen.gameState == gameScreen.playState) {
                g2.setFont(font25);
                g2.setColor(Color.WHITE);
                g2.drawImage(boxImage, gameScreen.fieldSize/2, 1, gameScreen.fieldSize, gameScreen.fieldSize, null);
                int boxNumber = 0;
                int levelNumber = gameScreen.currentBoard + 1;

                if (gameScreen.currentBoard == 0) {
                    boxNumber = gameScreen.gameObjectSetter.boxNumberL1;
                }
                if (gameScreen.currentBoard == 1) {
                    boxNumber = gameScreen.gameObjectSetter.boxNumberL2;
                }

                g2.drawString(" x " +  boxNumber, 74, 35);
                g2.drawString("Level " + levelNumber, gameScreen.fieldSize/2, 80);

                playTime += (double)1/60;
                marioCounter -= (double)1/60;
                g2.drawString("Time: " + decimalFormat.format(marioCounter), gameScreen.fieldSize * 6, 35);

                if (messageOn) {
                    g2.setFont(g2.getFont().deriveFont(30F));
                    g2.drawString(message, gameScreen.fieldSize / 2, gameScreen.fieldSize * 5);

                    messageCounter++;

                    if (messageCounter > 60) {
                        messageCounter = 0;
                        messageOn = false;
                    }
                }
            }

            if (gameScreen.gameState == gameScreen.pauseState) {
                drawPauseScreen();
            }

            if (gameScreen.gameState == gameScreen.titleState) {
                drawTitleScreen();
            }

        }
    }

    public void drawPauseScreen() {
        String text = "PAUSED";
        g2.setFont(font40B);
        g2.setColor(Color.WHITE);
        int x = getXForCenteredText(text);
        int y = gameScreen.screenHeight/2;

        g2.drawString(text, x, y);
    }

    public void drawTitleScreen() {

    }

    public int getXForCenteredText(String text) {
        int length = (int)g2.getFontMetrics().getStringBounds(text, g2).getWidth();
        int x = gameScreen.screenWidth/2 - length/2;
        return x;
    }
}
